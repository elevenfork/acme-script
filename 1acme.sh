#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1acme.sh' encrypted at Mon Jan 22 05:42:13 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SY���� �_������߯����������������� _Kc@t��f�       %� ���b#	DA2��i6�S'�SdL�4�i�O#SȌii�4�h2�h�  ��қSM���5�!�h#�a��	���20#  &j4� LL i� L"d�24=C�bFCM4dh� `@ @d�h@i��ѓ!CF�hd�ѐ��4ɣC&L�2124 �����2 �h0� "RM1M4�'��1�����4i���C@i�ꆀ�@h4�  h�=Fj� �h44�	����=L���'�M�zhmOSOS##I�~�a0��#L�$ɧZJ x�q�N�Xq1V�yW�q��r%\mԢC�J�����" ��=�
ˢ���W��l
��烧��ɚ�Ҝhf,��?|A\�2�����K1k�;�Q���2�cAT�T�I�ziVw|Pi�ժ��	O�:�"���5@_�eۿx�Xn�U�����xI��8��?&s�6c&��%�3�k_<2�`��8�M��{��q1�N�ǞF�3�{�K6=�԰{=X
��F��fd ӣ-��3��\�j֌��s'|NE�aq>�Mۊ��e��fe̦I+��nHNȆT�	�F���J!T;Y�"�Y;�����6ܯ5�%�F1)���G��D6	��2�W�&�,E%�9ŐD<��2�.�j�@�q46E$I���6!�`�/����Ҥ�됉�T�t̊|Pv��')��ɐ{�d<�z��^]@��c�x��H�jr�h_T��דw1X��̍r%ak�bV�9̥�(/+r{=
@�)HJ`���^l��Jq88����
��`t���y��g8���="h�K��c�7���Ah�c6X��!0Ty�:��H.���Xy),��0���Cp}��gc�nϓ���H�*H�;�/H���p��5^C�p�	� d�D������`�.�I�H�����^��n�n�aa�Ѽ��Z����6�z�;<N�,{c�y�g��g뙩����5�'&���lZ;
r6޳�ǭWr�>�(��5�}m��Բ{V�hm�"�J)��E��c8`�)%�d������&,xy�a�@A���3�8�W����>�)��v�R�ï���5+P/RG��jm.����4�Q}���P��emO{?�ϛ�u�k��C��9�c0���C�	P�T틭��Q��9�놣w7'�B���j��v�َh}=Sg�c�D,��$�4�,��sf{
B�R׮Wj���p�O5�1���U�,��	|�4�bq\�x�>�hZ0;0�a����$"�a0A@�>"�)Ղ��j�Q�H��ICl$��JC�D�9�" PL�@g&@�?m����0!D~��z�)Z)��Y���U�L�����("�"������G6v���y���\;dS�w�"dSjLQsXh8>�7�Y��V�{�|����{��˛~�\,�'��_]��p��%,h2K�Rń(��10:F��� id"��-n���� �<ml9�L!�Fo�����W�s��`�����iIi2���|
ϒil5C��h��8���D�@���37s{^ᑷV���� d��@yZB��}][���
�ėifi�ÙZ���@.J����a-��05xb�_0��<>��qɔ�-��9���P:�|�y�M���^%�L�5ǐ�Ea��;ԞcXI�J�����1��<�&�X dD� !wfa3����>���@ᗈ(�E����D���$h�r��<
\E�Ė�,8L%IYYd�+�e'�9|[�o�l�A�0)�S��:D�)&R�/��I(����|�+g@,?Y	\��������������DG�Nkу�z`����✍�&���v/t��ʒ�7�b�q{~/ذ^uꁳX!��Ŝ!���x_�78
ld�Q7煙Um��'r��g=�}v7bRp6J3�JsC�s2����ʶ�ϋ=�{1����V��D�l4ڿJ����s�.�6{�A�.�__����#��!K �쵯�>
��f�����bf���#�6�]<A�^^�+�.Q"��?)���h�<k�D6ҥ"�Ȣ����)�y��)"�i�|t��.@0M�l�B �/�i�<3h�y��Ǘ�_��6=��� F @�$@w&��ǜW)JB�4g2b�v�=@|車����h9 �ʽ� ��2�f���zCY�H�&?�ߑz �CI(�%AT"�n�	I���Ո���KBh3�G߃�L�'��hV�`���_Xc8EzJ�lд��(��3���Q~�A�B¦��3
?0#�d�h^����ң �T��JZ5P<�{
1�.���W��.��/�ҚM͘ ӸI�	���vʦrFy$��2�)z��0-!��(_hMT.W@�3M/��8��� �l��~�cSX�$&�@0�C��.k�4�)$������!�}�N�pĽ��G0����2��0c_k�	c�j��i�5�(j��v�7Z�_m� �b@��3�8̡��_vVr (��8��a�Ѡӧ���l�o�M�HY��
�E�z�`(-v�����؟8b�U�+H��	�9�����ҕ�.E�Hhx�3`�X��B8��$� ��0E�2�a���+�.��*B���5��"B\I% �Tʂ�ިb<�q��@PCJ��`h�T��Z��D�2�j/S@�i��W �Ja�00m. >�i�(��r[̡N�*�H�D@��RH�_��i �/+S�8�r>w�x����dda^0�b'0]b� ���,� K��0 �W��1tXз�B:�
1rA��$��XfR@t$%� ��2�QJsA��H�8��r��i�&P�ZB^+̋ZWll��Cc2���&�n�l1"���ش��a=nJ�@8sZQx�p�1��Mf;��7֔�����f %�T[N5Č X0�q�B��K�J��O=���(
Iqu,�Md�,#X�BT ���ɒ@�Ӏi�H\�Jg ـ�%p��}�'�(%�"D�ril4����K�V�aG����y���.�z��%�7�.�c[ pg�>�=�?ɔA K��dW�����.O�Z�ʒ��2���j2����Dˬ8l�I+��NV�i&Q!, ���D�5���T��' �E@hD�Nlid��*� u�$ƕ��B,*x�5Xi�yPϗ�.B�J�B!���	��t ����V���X�3L3��w�d�Kڍ���?V CR@H�șT�[R��4��bFY����	b�4�r7�	)���ʃ��١&��)��hۼ�1&n���A�7G ��c�Y��=�<����b���N�t���I���F�^�����-��o��%��� Hlf6^2V��/2�:��I�Z31�XRK�3-��+2�r��{���v�LZ/
!A�|���!_��<j�-�
��	LR$��p�&c+�'#X�)�~C�lį��-kX!0�ԖX*Ă�p2�P�(b��$	�t�7� �K����l��.�k4#9؋�F�*��Y>��De�ٗ�"Y�P��VM,�)��"B��B��QAhE�C�Z	a�i$��Sh$�X������)�"��6�r�eA�h	�MfA��+jH�9�K'e� ���/Q)��,)��� �-h=�uQ��3a�u6�2oqL���Z�����@�8��#�P� %9`\�jE�h�w����%1�4E��0��a`�c|c��/F �������G�� �-AF��^�X|�����FN���4�Ԓ�5��#�:��.m�XE��K���Q��,�G�!� ��؁�c���;X>ad����3dd�E�lB��i��\��< �wk�J�!!c�p���:A���*���A�p���OtU��V��n�L�mEFM���r"Lq��C�I�f��@����D!wǨK!mjJ�7YŮ���@��To��*j�����>��͔ �$Z�jjM�5Pl��LlF#������5�cB���N���_ϼ���v������]��$.E�n�ۑAQ�cF��1�C+ ����)Jhȝ��i`jJc����D�$����d�� T&�9�n2	I��,G(A˿
R�E�j�V�����kܑ��P��y��je;H�� �@T̢CU�E§pe��������4�!�럁�� 3J$���H�a�[�m@�XL@�ai�A��dc��1)	P B�
Pkt�I�Ο�����aI"�O�TI{�Llt"������2 �%�o#ٿG��@8���l�T���B��#�`aD�����Ӄ�>
�Ҝ���,�đ�IL^6�kkL�����jp���G*���7M���F�j��8�"�a��#��IIµH��1O(�O� gk͂đk҅t-u}#��ZH'ӳ�Ԓ0'{X� }�~�`ol�Φo�J%��w�R��<F�"��q2 �C�J6�L`�jD���.:sL�x5��W���%�"B�5�\�,�H�k���'�����i1p#�B���Z�~�`6&!�������N�� � ���z���w�����B� ��u˙s�Z�P���Ƽ�A�Lm�г݂M��cĘ���?��|v*���c���q������_j�إ���d�͝AS;,�ii�XC p�K��.��*x^0��
EՀ1��f`�܊�mH"�/�U����ھm�.gu0�!�A�x<p�y�"��x����u+N���lP�𔽿-�qx��9��NZ����N��l���~/⑛���0������ �P*O��8�$)*����{T�����`.��m�a*�d��\�3I�g藎�v��G�cz���ݬ$s	o�T��.�2jҎ7C~�x;�&�㓲ɗ�aBAj;�7op�H;��7c.���+� 쓠�H�.���b����*�2hZͱc�;	fV���1xJ�-]�k����Um�8����m�Z�ɹC�u��y��e��7�rН�2����M������EF���h'IE��dʡR)P>�Ќ'[����|�[>��AIQ2�X�=(��׉�TN��Ɠ����j�<��j������I	��%h�b��GF�ʣ��Ep���6!�5�*�	��ҿ�BV��ze���% �Tw��0����#L�7��ɲf�8�;.>{���-RL�:�sV��(���OY�N��%����KY!�����aȻz�<~QRM����](O��#,�)��o��g�,[��"�!%`@�)�b�_��<�����-�>���9�3��&�:\��(J�Υ�}{�-��&�%�I`ޤaD�v)�-~�­��ѿ��X��
��Y�m�����}���6q���C�z~&�1G���+��:�]�Jr�ÃBL��:a��
�� a��s���<R�0�]�rKz;>v9�ܑN$"��*@